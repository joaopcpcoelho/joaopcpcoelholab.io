---
layout: book
title: Why we sleep - by Matthew Walker
ISBN: 978-0141983769
date_read: 31-7-2018
recommendation: 10


---

Very insightful book on all things related to sleep, well supported by relevant research - not just by the author but seemingly by most scientists invested in the subject. Both informative and easy to act upon. After a few chapters the spin is very predictable - whatever the issue, more sleep helps - but still worth a read simply because it is not expected that sleep will have an effect on so many aspects of our well-being. 


<!--more-->

Two main factors determine when you want to be asleep or awake: your internal twenty-four hour clock (circadian rhythm) and a chemical substance that builds up in the brain during wake-time and creates a sleep pressure (adenosine).

Everyone generates a circadian rhythm.

The circadian rhythm controls a bunch of body patterns, including preferences for eating and drinking, moods and emotions, amount of urine produced, core body temperature, metabolic rate and release of various hormones. 

It is no coincidence that the likelihood of braking an Olympic record has been clearly tied to time of day, being maximal at the natural peal of the human circadian thythm in the early afternoon.

De Mairan, 1729: plants generate their own internal time, independently of exposure to sunlight.

Mamooth cave experiment: in 1938, two scientists stayed for 6 weeks at Mamooth Cave, Kentucky, in total darkness, to see whether circadian rhythm was internally generated or depended on exposure to sunlight. Two conclusions: human circadian rhythm is generated internally, in the absence of external light from the sun; and circadian rhythm is approximately, but not exactly, one day - in fact it's slightly longer (later experiments: 24 hours 15 minutes). 

This 24 hour biological clock is the suprachiasmatic nucleus. Temperature, wakefulness and sleep are examples of 24-hour rhythms that the suprachiasmatic nucleus controls.

An adult's owlness or larkness - their chronotype - is strongly determined by genetics.

Owls treated unfairly by society: labeled lazy, when they're genetically bound to a delayed schedule; and must adapt to society's work scheduling which is strongly biased toward early start times that punish owls and favor larks.

Why this variability? Humans likely evolved to co-sleep as families or tribes rather than individuals. So variability in sleep times means the group as a whole is vulnerable for a shorter time despite everyone getting the same amount of sleep.

Suprachiasmatic nucleus communicates by segregating melatonin. 

Melatonin helps regulate the timing of sleep but has little influence on the _generation_ of sleep.

The placebo effect is the most reliable effect in all of pharmacology.

It feels harder to acclimate to a new time zone when traveling eastward then when flying westward. The eastward direction requires that you fall asleep earlier than you normally would. The westward direction produces a longer day which is closer to the natural circadian rhythm.

Effects of jet lag on cabin crews: parts of their brains, those related to learning and memory, physically shrunk; and short-term memory was significantly impaired.

Your twenty-four hour sleep cycle is the first of two factors determining wake and sleep. The second is sleep pressure. 

Sleep pressure - the increasing desire to sleep - is a consequence of adenosine accumulating in the brain.

When adenosine concentrations peak, an urge for slumber will take hold. This happens to most people after 12 to 16 hours of being awake.

Caffeine works by successfully battling with adenosine for the privilege of latching on to adenosine receptors in the brain.

De-caffeinated does not mean non-caffeinated.

Caffeine wears off by action of an enzyme in the liver. Based in large part on genetics, some people have a more efficient version of the enzyme that degrades caffeine and clear caffeine faster. 

Aging also alters the speed of caffeine clearance: the older we are, the more sensitive to caffeine's sleep-disrupting influence.

Caffeine crash: energy levels plummet once caffeine is cleaned from the system. Why? While caffeine is latched to receptors, adenosine is not - but it continues to build up. Once the receptors are clear, more adenosine than before is ready to latch on.

Effects of drugs on spider web building: caffeine worse than LSD, speed and marijuana. Study: Noever, Cronise and Relwani, "Using spider-web patterns to determine toxicity". Good option for TreeTree2 summer academy.

Am I getting enough sleep: no if you can fall back asleep at 10 or 11 after waking up in the morning, or if you cannot function optimally without caffeine before noon.

Time dilation during dreams: tests in rats learning a maze showed the patterns of brain-cell firing that showed up while they were learning the maze repeatedly showed up during sleep. Also, during REM sleep memories were bein replayed at just half or a quarter of the speed of that measured when rats were awake and learning the maze. Evidence for dream time.

REM sleep "intimately connected to the experience we call dreaming".

Sleep cycle: the cerebral war between NREM and REM sleep is won and lost every 90 minutes.

{% figure sleep_cycle jpg 'Sleep cycle overview. Source: https://www.howsleepworks.com/types_cycles.html' %}

Different species have different NREM-REM cycle lenghts. Most shorter than humans. 

The ratio of NREM to REM sleep within each 90 minute cycle changes dramatically across the night. Earlier more NREM heavy, later more REM heavy. Why is it so? No scientific consensus. Author hypothesis is that it is a way of handling the limited storage capacity. Analogy of the sculptor: start with block of raw material, then remove huge chunks (NREM) and do some early details (REM), then remove increasingly smaller chunks and introduce increasingly greater detail. 

This asymmetrical sleep cycle is also a way of making sure you get at least some NREM and REM sleep, even in the event of a short night. From an evolutionary perspective this is a safeguard against waking up mid-sleep to escape predators etc.

This NREM-early, REM-later profile has a danger: if usual sleep schedule is 00-08, then if one day you wake up at 6 you lose 25% of total sleep time but 60-90% of REM sleep. Similarly, if going to bed at 2 rather than 00, then you lose more than 25% of NREM sleep.

NREM and REM are equally important - both serve critical, though different, brain and body functions. NREM is associated with memory retention - moving memories from short to long-term storage - and REM is associated with interconnecting those memories.

How is brain activity measured? Brain electrical activity. Full wakefulness: fast-frequency, chaotic brain waves. REM sleep: very similar to full wakefulness. NREM sleep: low-frequency, synchronous, predictable waves.

Sleep spindles: bursts of brain activity at the tail end of NREM sleep waves. The more powerful and frequent they are, the more resilient we are to external noises that would otherwise awaken us.

Deep-sleep brainwaves are generated in the middle of the frontal lobes and travel from the front of the brain to the back. 

In the 1950's and 1960's the assumption was that this slow, lazy-looking electrical pace of brainwave activity must reflect a brain that is idle or dormant. That is wrong - in fact deep NREM sleep is "one of the most epic displays of neural collaboration that we know of".

REM sleep brain activity is an almost perfect replica of that seen during alert wakefulness - in fact there are individual parts of the brain that are up to 30% more active during REM sleep than when awake. So what information is being processed during REM sleep, since it's not information from the outside world?

Each night REM sleep treats you to a "bizarre, highly associative carnival of autobiographical themes". Signals of emotions, motivations and memories are played out in the big screens of our visual, auditory and kinesthetic sensory cortices in the brain. 

Wake state is reception of information, REM sleep is integration of information (association and interconnection of raw ingredients, promoting innovative insights and problem-solving abilities).

During wakefulness there is some degree of muscular tension, even when lying in bed relaxed. When passing to NREM sleep some of that tension disappears but much remains. However, before REM stage starts, your body becomes completely paralized and there is no tone in the voluntary muscles. Involutary muscles continue to operate normally, voluntary muscles become lax.

Why are muscles paralized during REM sleep? Because this prevents us from acting out our dream experiences.

REM means Rapid Eye Movement. This rapid eye movement (from side to side) was thought to correspond to the tracking of visual experience in dreams, but that's not the case.

My own comment: sleep research looks very much like earlier astronomy, physics etc. Very early staged, as of 1950's, lots of assumptions that are being methodically tested and to a large degree dismistified.

Sleep is universal and old: it emerged at least 500 million years ago (during the Cambrian explosion).

We usually think of sleep as the state to fix what was upset by wakefulness. But an alternative perspective is that sleep was the first state of life on this planet and it was from sleep that wakefulness emerged. 

Four key differences in sleep from species to species: total sleep amount, composition (stages of sleep), way of sleeping, and sleep patterns.

Total amount of sleep time varies a lot among organisms. Not clear why - likely contenders (body size, prey/predator status, diurnal/nocturnal) don't explain observations. One "somewhat meaningful" predictor is brain complexity relative to body size. But this is not the whole story - for example the opossum, same weight as a rat, sleeps 50% more.

Best guess for sleep amount based on a combination of dietary type (omnivore, carnivore, herbivore), predator/prey balance, presence and nature of social network, metabolic rate and nervous system complexity.

Every species measurable shows NREM sleep, but only birds and mammals show REM sleep. This suggests REM is the most recent evolutionary development. However certain cetaceans don't show signs of REM sleep so we can't say it exists for all mammals.

Birds and mammals developed separately so it seems REM was birthed twice during evolution (similar to eyes). However there is an australian lizard that predates them both and seems to have a proto form of REM sleep - if so then it's likely that REM in birds and mammals evolved from this common seed.

Which type of sleep is most important? Tested by depriving individuals of each type of sleep for one night and inspecting what happens the next night. First: sleep duration is far longer on the recovery night than on a normal night (10-12 hours, compared to normal 8). Second: NREM rebounds harder - more NREM than REM on recovery night. Third: on second, third and fourth nights, there's a reversal - more REM than NREM. Similar to the night pattern. We try to recover NREM first but the brain will try to recover both, so they're equally important.

Regardless of recovery opportunity, the brain never comes close to recovering all the sleep it has lost. *Humans can never sleep back what has been previously lost.*

Species with non-stop aquatic movement still sleep, one brain half at a time. Humans have a very mild version of unihemispheric sleep. 

REM sleep cannot be split among brain halves - birds sleep with both halves of the brain during REM sleep, humans as well, every other species as well. Cetaceans don't have REM sleep.

Response to extreme environments: individuals who are deliberately fasting will sleep less as brain is tricked into thinking that food has become scarce; female killer whales and their newborns will enter joint sleep deprivation while the calf is vulnerable; birds during transantlantic migrations will sleep for only seconds at a time. 

Sparrow is almost invulnerable to sleep deprivation during migration period, but extremely vulnerable outside it.

Most adults sleep in a monophasic pattern, but modern hunter gatherer tribes sleep in a biphasic pattern.

Sleep bankrupcy: result of delayed go-to-bed hours (from dusk to midnight) and absence of afternoon nap.

Practice of biphasic sleep is not cultural but deeply biological. All humans have a biologically hardwired dip in alertness in the midafternoon hours (the post-prandial alertness dip). 

Don't book presentations for after lunch.

The true pattern of biphasic sleep (with anthropological, biological and genetic evidence) is a longer bout of continuous sleep at night followed by a shorter midafternoon nap.

Greek experiment: before 2000, pressure to abandon siesta. Researchers followed cardiovascular outcomes on a group of 23000 adults over a 6 year period, where most stopped having a siesta. None of the individuals had a history of cardiovascular problems but those that abandoned siesta had a 37% increased risk of death from heart disease across the 6 year period. In workingmen, the mortality risk of not napping increased by 60%.

*When we drop the innate practice of biphasic sleep, our lives are shortened.*

Humans: 20-25% sleep time is REM sleep, compared to 9% average for other primates - we're the anomaly. Why: part of the answer is fire: allowed us safety to sleep on the floor, thereby in greater safety, thereby increasing the intensity of sleep (increase the amount of REM sleep).

REM increases our ability to recognize and navigate socioemotional signals. The adaptive benefits of complex emotional processing are monumental and often overlooked.

As a result of REM sleep, we can awake the next morning with new solutions to previously intractable problems or even infused with radically new ideas.

Self-fulfilling positive cycle in evolution: fire allowed for moving from tree to ground; that gave us longer and more secure sleep, and thus more REM sleep; that gave us more creativity and emotional intelligence, and thus social complexity; this, along with increasingly dense brains, led to improved survival strategies; and the more we developed the circuits of the brain during the day, the greater the need for REM sleep during the night. As this feedback loop took hold in exponential fashion we formed, organized, maintained and deliberatively shaped ever larger social groups.

In fetuses wakefulness only appears in last trimester, 2-3 hours a day. Total sleep time decreases in this stage but REM sleep increases - in the last week before birth it hits a lifetime high of 12 hours a day. During this time the brain is creating the links between synapses - synaptogenesis.

Experiments in rats and other mammals have shown that if REM sleep is supressed at this stage, gestational development is retarded. If it is later allowed, development restarts but does not accelarate nor does it get back to normal pace - _an infant brain without sleep will be a brain ever underconstructed_.

Autistic individuals show a 30 to 50 percent deficit in the amount of REM sleep they obtain (evidence in humans is simply correlational and not causational though).

Alcohol is a powerful REM sleep suppressor. Newborns of heavy-drinking mothers spent significantly less time in REM sleep that non-drinking mothers' children. The quality of their REM sleep was also worse - about 200% (1/3) reduction of electric activity.

Heavy drinking is not required - children of mothers who had 1-2 glasses of wine also suffered dampened REM sleep intensity. Moreover their breath rates dropped from 381 per hour to 4 per hour. _381 to 4_.

Beyond pregnancy, alcohol also has an effect during breastfeeding period. 1-2 glasses of wine before breastfeeding result in 20-30% suppresion of REM sleep in children.

During womb-stage and early childhood years REM sleep dominates. After birth, NREM gradually gains prevalence over REM: at 6 months the split is 50/50, at 5 years old 70/30, and around late teen years it stabilizes on 80/20. Why? First we're building all neural connections (REM), then we're pruning unused connections (NREM) in order to enhance the ones left over.

Changes in deep NREM sleep precede development milestones by a few weeks or months, suggesting a causal relation: deep sleep may be a driving force of brain maturation, not the other way around.

Discovery by I. Feinberg: brain maturation does not happen at the same time across the brain. Instead, the rise-and-fall patterns of maturation always began at the back of the brain (visual and spatial perception) and then progressed forward. The very last stop on the maturational journey was the tip of the frontal lobe - which enables rational thinking and critical decision making. This helps explain why rationality is ojne of the last things to flourish in teenagers.

Challenges for adolescents getting enough sleep: a change in their circadian rhythm and early school start times. 

During puberty the circadian rhythm moves forward (irrespective of culture or geography), passing the circadian rhythm of parents. As a child 9pm is sleep time, in adolescence it is peak wakefulness still. 

Asking a teenager to go to bed at 10 is the equivalent of asking an adult to go to bed at 8; asking them to wake up at 7 is the same as asking an adult to wake up at 5.

Why this change in circadian rhythm? Author proposes a socio-evolutionary one: pushing sleep times later allows for operating independenlty for a short time every night, easing the transition from parental dependence to independence (and furthermore it does so as a peer-group collective - for all teenagers at the same time).

That older adults need less sleep is a myth.

Key changes in sleep as we age: (1) reduced quality and quantity; (2) reduced sleep efficiency; and (3) disrupted timing of sleep.

Quantity and quality of deep NREM sleep declines over time, starting around late-twenties. At late-forties we have 60-70% less deep NREM sleep than as a teenager, and at 70 less 80-90%.

Sleep fragmentation: as we age we wake up more often during the night (chief reason: weakened bladder), decreasing amount of time spent in bed actually sleeping (sleep efficiency). Sleep efficiency: 90% is good (45m awake in an 8-hour night) and we're at 95% as teens, at 70-80% as 80-year-olds.

In older people, circadian rhythm pushes earlier and earlier. This means a nap in the couch is likely, and problematic because it releases adenosine, meaning it's then harder to fall back asleep. Furthermore, irrespective of how much you've slept, at 4 or 5 the circadian wakefulness kicks in making it harder to stay asleep.

In older adults, sleep debt is a cause, or at least strongly linked, to dementia.

Sleep is not the absence of wakefulness.

Sleep before learning refreshes our ability to initially make new memories.

The more sleep spindles an individual has at night, the greater the restoration of overnight learning ability in the next morning.

Seniors (60-80) are 40% less able to generate sleep spindles as young adults.

The concentration of NREM sleep spindles is higher in the late-morning hours, sandwiched between long periods of REM sleep. So sleep 6 hours or less and you're depriving the brain of much of sleep's learning restoration benefits.

The slow brainwaves of NREM serve as a courier service, transporting memory packets from a temporary storage (the hippocampus) to a more secure, permanent home (the cortex).

Following a night of sleep, you regain access to memories that you could not retrieve before sleep.

Brain stimulation: boosting deep NREM waves makes sleep more effiecient, e.g. increasing memory capacity. Stimulation can be electrical, acoustic or even physical (bed suspended in ropes swinging).

Selective memory enhancement: by going over a list of "things to remember before sleep" and associating a sound with each one, then replaying the sound at night (without waking up the subject), subjects remember reactivated items far more than those that were not.

Same thing for selective forgetting: by tagging some items with "remember" and some with "forget" before sleep, we see a selective boosting of the "remember items" only.

Sleep is far more intelligent that we once imagined.

Piano man: "I can just play" the morning after, even if failing before. The brain continutes to improve skill memories in the absence of any further practice. 

It is practice, followed by a night of sleep, that leads to perfection.

Sleep helps the brain automate the movement routines, making them second nature.

The increases in speed and accuracy were directly related to the amount of stage 2 NREM, _especially in the last 2 hours of an 8-hour night of sleep_. It was the number of sleep spindles in the last 2 hours of late morning that were linked to the offline memory boost.

Sleep spindles bathe all parts of the brain, but a disproportionate emphasis is placed on the parts of the brain that have been worked hardest with learning during the day.

Sports: sleep important before a game but equally, if not more, important in the days after a game - accelarates recovery from common inflamation, stimulates muscle repair, and helps restock energy.

Sleep patterns in the NBA: interesting topic for a project.

Creativity: sleep provides a nighttime theater where the brain tests out and builds connections between vast stores of information. Importantly, this task is biased towards seeking the most distant, non-obvious connections - thus contributing to problem solving abilities.

Sleep is amazing. Key takeaways: (1) take naps, and (2) get the full 8 hours every night.

One brain function that buckles under even the smallest dose of sleep deprivation is concentration.

Sleep deprivation causes slower reaction times but also _microsleeeps_: brief moments when we stop responding altogether.

Partial sleep deprivation:
- 4 hours per night during 6 days: equivalent of 24h straight with no sleep
- 4 hours per night during 11 days: equivalent of 48h straight with no sleep
- 6 hours per night during 10 days: equivalent of 24h straight with no sleep

You do not know how sleep-deprived you are when you are sleep deprived. 

With chronic sleep restriction over months or years, that low-level exhaustion becomes their accepted norm.

Sixty years of scientific research prevent me from accepting anyone who tells me they can "get by on just 4 or 5 hours of sleep a night just fine" (sorry Marcelo).

Sleeping it off does not work - after being sleep deprived and having had 3 nights of recovery sleep, no group recovered all the sleep hours they had lost. The brain is incapable of that. Stark constrast with Nick Littlehales' book.

After being awake for 19 hours, people were as cognitively impaired as those legally drunk.

Being drunk and sleepy has a multiplicative effect rather than additive. Individuals with 4 hours of sleep plus legally drunk had 30x more accidents than the well-rested, sober group.

Bunch of relevant insights:
- the recycle rate of a human being is around 16 hours (cognitive ability starts declining after 15 hours awake).
- humans need more than 7 hours of sleep each night to maintain cognitive performance.
- after 10 days of just 7 hours of sleep the brain is as dysfunctional as it would be after going without sleep for 24 hours (although earlier in the book the author says 6 hours - so not sure). 
- three full nights of recovery sleep (i.e. a weekend) are insufficient to restore performance back to normal levels after a week of short sleeping.
- the human mind cannot accurately sense how sleep-deprived it is when sleep-deprived.

When a truck drived loses his life in a drowsy-driving crash, he will on average take 4.5 other lives with him.

If you are drowsy while driving, _please, please stop_ - it is lethal.

Power naps may momentarily increase basic concentration under conditions of sleep deprivation, as can caffeine up to a certain dose. But neither naps nor caffeine can salvage more complex functions of the brain, including learning, memory, emotional stability, complex reasoning, or decision-making.

The amygdala - a key hotspot for triggering strong emotions, and linked to the fight-or-flight response - showed well over a 60 percent amplification in emotional reactivity in the participants who were sleep deprived.

It was as though, without sleep, our brain reverts to a primitive pattern of uncontrolled reactivity.

After a full night's sleep, the prefrontal cortex - the rational, logic and decision-making center - was strongly coupled to the amygdala; without sleep, this coupling is lost. 

Without the rational control given to us each night by sleep, we're not on a neurological - and hence emotional - even keel.

Emotional shortcomings show up even after restrictions of only sleeping 5 hours for 5 nights. 

Insufficient sleep does not push the brain into a negative state and hold it there. Rather, the under-slept brain swings excessively to both positive and negative emotional extremes.

Negative mood swings can lead to depression and suicidal thoughts - one more reason to foster enough sleep in teens.

Extreme positive mood swings can lead to sensation-seeking, risk-taking and addiction. 

Insufficient sleep linked with addiction relapses, due to reward cravings that are unmetered.

Insufficient sleep during childhood significantly predicts onset of drug and alcohol use.

There is no major psychiatric condition in which sleep is normal.

Sleep disruption remains a neglected factor contributing to the instigation and/or maintenance of numerous psychiatric illnesses, and has powerful diagnostic and therapeutic potential that we are yet to fully understand or make use of.

Depression is not only about excess negative feelings but also has much to do with the absence of positive emotions (anhedonia): the inability to gain pleasure from normally pleasurable experiences, such as food, socializing or sex.

In treating depression, sleep deprivation proves harmful to most individuals but beneficial to about 1/3 of them. Why? One possible explanation is that those individuals experience greater amplification within reward circuits of the brain, resulting in stronger sensitivity to positive reward triggers.

Is pulling an all-nighter a good idea for learning? Experiments showed 40% deficit in the ability of sleep-deprived individuals to make new memories, i.e. learn. Any new incoming information was simply being bounced by the brain. 

A whole night of sleep deprivation is not even necessary: simply disrupting the depth of an individual's NREM sleep with infrequent sounds, preventing deep sleep and keeping the brain in shallow sleep, without waking the individual up will produce similar brain deficits and learning impairments.

Memories formed without sleep are weaker memories, evaporating rapidly.

Theories, beliefs and practices die one generation at a time.

These realizations have sparked a change in classes by the author. Instead of exams in the end he now splits courses up into thirds so that students only have to study a handful of lectures at a time. (this was helpful for me in university learning PE and quantum mechanics for example).

If you don't sleep the very first night after learning, you lose the chance to consolidate those memories, even if you get lots of catch-up sleep thereafter.

Sleep is not like the bank - you cannot accumulate a debt and hope to pay it off at a later point in time. _Sleep for memory consolidation is an all-or-nothing event._ It is a concerning result in our 24/7, hurry up, don't wait society.

A lack of sleep is fast becoming recognized as a key lifestyle factor determining whether or not you will develop Alzheimer's disease.

Sleep disruption and Alzheimer's interact in a self-fulfilling negative spiral that can initiate and/or accelarate the condition.

ALzheimer's disease is associated with the buildup of a toxin called beta-amyloid. Amyloid accumulates in the middle part of the frontal lobe - the same brain region essential for the electrical generation of deep NREM sleep.

Despite Alzheimer's being typified by memory loss, the hippocampus - the key memory reservoir in the brain - is unaffected by amyloid protein. So where does forgetfulness come from? Individuals with highest levels of amyloid build-up had the most severe loss of deep sleep and as a consequence failed to successfully consolidate those memories. _Overnight forgetting, rather than remembering, was taking place._ The disruption of deep NREM sleep was therefore a hidden middleman brokering the bad deal between amyloid and the memory impairment in Alzheimer's disease.

Glymphatic system: a kind of sewage network that collects and removes dangerous metabolic contaminants generated by the hard work of neurons in the brain. 

The Glymphatic system is somewhat active during the day, but it kicks into high gear during deep sleep - increasing effluent expulsion from the brain by 10-20x. This is because the glial cells shrink up to 60% during the night, enlarging the space ("streets") around the neurons and allowing the cerebrospinal fluid to better clearn out the metabolic leftovers.

One of this toxic metabolic leftovers is amyloid protein - so the more deep sleep one gets, the least vulnerable to Alzheimer's.

Wakefulness is low-level brain damage, while sleep is neurological sanitation.

Getting too little sleep across the adult life span will significantly raise your risk of developing Alzheimer's disease.

Margaret Thatcher and Ronald Reagan, very vocal about sleeping only 4-5 hours a night, went on to develop Alzheimer's (interesting to see if it happens with Marcelo).

Can we begin supplementing the declining deep sleep of vulnerable members of society during midlife, many decades before the tipping point of Alzheimer's disease is reached, aiming to avert dementia risk later in life? Similar to what is already done in medicine to prevent cardiovascular diseases.

The shorter you sleep, the shorter your life.

Adults 45 or older who sleep fewer than 6 hours a night are 200% more likely to have a heart attack or stroke during their lifetime, compared with those sleeping 7-8 hours a night.

Important to prioritize sleep in midlife - unfortunately the time when family and professional circumstances encourage us to do the exact opposite.

Mechanisms by which sleep deprivation degrades cardiovascular health are numerous but seem to cluster around a common culprit, the sympathetic nervous system (that triggers the fight-or-flight mechanism). This system is intended for short-term responses under threats. Leaving the system on for long durations of time leads to multiple health issues.

For as long as the stae of insufficient sleep lasts, and for some time thereafter, the body remains stuck in some degree of a fight-or-flight state.

Daylight savings time: hospital records show that the day after hour change in March (when most people lose 1 hour of sleep), heart attacks spike. Also, the day after hour change in October (most people gain 1 hour of sleep), heart attack rates plummet.

The less you sleep, the more likely you are to eat. Chronic sleep deprivation is now recognized as one of the major contributors to the escalation of type 2 diabetes.

Getting enough sleep will help control body weight. A full night's sleep repairs the communication pathway between deep-brain areas that unleash hedonic desires and higher-order brain regions whose job it is to rein in these cravings.

Diets and sleep: one experiment set people on a low-calorie diet and split them into two groups, one with 5.5 hours of sleep and another with 8.5 hours of sleep. Both lost weight but the type of weight loss was very different: those sleeping 5.5 hours lost 70% muscle, and those sleeping 8.5 hours lost under 50% muscle (the rest being fat in both cases). 

Short sleep will increase hunger and appetite, compromise impulse control within the brain, increase food consumption (especially high-calorie foods), decrease feelings of food satisfaction after eating, and prevent effective weight loss while dieting. 

Key aspects of the human reproductive system are affected by sleep in both men and women.

Exposed to common cold, people who had been sleeping 5 hours on average had an infection rate of 50%, while those sleeping 7+ hours had an infection rate of 18%.

Denmark recently became the first country to pay worker compensation to women who had develloped breast cancer fter years of night-shift work in government-sponsored jobs.

We now know that a lack of sleep will encourage cancer-spreading and -amplifying processes.

Poor sleep quality increases the risk of cancer development and, if cancer is established, provides a virulent fertilizer for its rapid growth.

Chronic sleep loss will erode the your genetic code and the structures that encapsulate it (wow).

Neglecting sleep amounts to deciding to perform genetic manipulation on yourself each night, tampering with the nucleic alphabet that spells out your daily health story.

Bottom line: sleep loss is related to worse health all around.

The prefrontal cortex, the _CEO of the brain_, manages rational thought and logical decision-making. It is this region of the brain that is temporarily each time you enter the dreaming state of REM sleep.

Scientists have been able to predict with significant accuracy the content of participants' dreams at any one moment, basically using MRI reports and dream reports obtained from test subjects and matching those MRI patterns. 

A question looms: when we can technically read our dreams, do we hold the dreamer responsible for what they dream?

Freud introduced the idea that dreams come from the brain.

Freud's theory is a "quagmire of unprovability", because of the lack of any clear predictions.

A meaningful, psychologically healthy life is an examined one, as Socrates so often declared.

Since autobiographical memory regions of the brain, including the hippocampus, are so active during REM sleep, we should expect dreaming to contain elements of the individual's recent experience. However, this is not the case - but there is a strong and predictive daytime signal in the static of nighttime dream reports: emotions.

Function of dreams: nursing our emotional and mental health, and enabling problem-solving and creativity.

The dream stage of REM sleep removes the emotional load from memories. But it was shown by Cartwright that only those patients who were expressly dreaming about the painful experiences around the time of the events went on to gain clinical resolution from their despair. In other words, it is not enough to have REM sleep, or even generic dreaming, when it comes to resolving our emotional past - we need dreams that expressly involve the emotional themes and sentiments of the waking trauma.

The better the quality of REM sleep across a rested night, the more precisely individuals were able to decode facial expressions the next day.

REM sleep for problem solving: when trying to solve problems containing a hidden shortcut, 20% of individuals without sleep found it, compared with 60% of individuals who had had a full night's sleep.

Lucid dreaming occurs the moment an individual becomes aware that he is dreaming. Some individuals can control what they are dreaming about and even direct the experience towards a desired function e.g. problem solving. The existence of lucid dreaming has been demonstrated via MRI scans awake and asleep matching individuals signals with pre-defined eye movements - the only muscles spared from REM paralysis.

Sleep deprivation is not insomnia. Sleep deprivation: having the ability to generate enough sleep but not giving oneself enough opportunity to sleep. Insomnia: lacking the ability to generate enough sleep despite giving oneself enough opportunity to sleep.

Two types of insomnia: onset (difficulty falling asleep) and maintenance (difficulty staying asleep).

The sleep aid industry is worth 30 billion dollars a year in the US.

The two most common triggers of insomnia: emotional concerns (worry) and emotional distress (anxiety).

Chronic activation of the fight-or-flight mechanisms causes multiple problems, including insomnia. Why: (1) raised metabolic rate results in higher core body temperature, (2) higher levels of hormone cortisol and sister neurochemicals adrenaline and noradrenaline result in increased heart rate, (3) emotion-generating and memory-recollection centers in the brain remain active.

Simply put, insomnia patients cannot disengage from a pattern of altering, worrisome, ruminative brain activity.

Patients with insomnia not only have less quantity of sleep, they have also worse-quality  sleep (shallower deep NREM sleep plus more fragmented REM sleep).

Sleeping pills: "blunt instruments" tha simply sedate the cortex (similar to what alcohol does), and are no longer recommended as the first-line treatment approach to insomnia.

Narcolepsy: a neurological disorder with symptoms (1) excessive daytime sleepiness, (2) sleep paralysis, and (3) cataplexy. In a cataplexic attack a patient suffers the demolition of all muscle tone, resulting in an immediate collapse on the spot. Moreover these attacks are triggered by moderate or strong emotions, positive or negative. 

Narcolepsy is caused by deficient production of orexin. Orexin is the neurotransmitter that communicates to the hypothalamus that it's time to turn the sleep switch on or off. Narcoleptic patients show a loss of almost 90% of orexin-producing cells.

Treatment outlook is bleak: amphetamines for symptom 1 (less than ideal - addictive and affecting multiple areas of the brain) and antidepressants for symptoms 2 and 3 (marginally effective).

Fatal familial insomnia is a genetic disease that renders an individual unable to sleep and eventually kills him. It's triggered by a genetic mutation that alters a protein so that it behaves like a virus, destroying large parts of the brain, one of which is the thalamus (the sensory gate that must close for sleep to be generated). Traced back to a venetian doctor in the late 18th century.

Fatal familial insomnia is the strongest we have that a lack of sleep will kill a human being.

Results of sleep deprivation experiments: rats will day after 15 days without sleep. Total sleep deprivation resulted in death as fast as total food deprivation did. Total REM sleep deprivation resulted in death almost as fast as total sleep deprivation. Total NREM sleep deprivation still proved fatal but took 3x longer.

Effects on rats: (1) began losing body mass, (2) lost the ability to control body temperature (metabolizing from the inside out), (3) sores on skin and wounds on paws and tails (immune system collapsing) and decrease in size and weight of ınternal organs. Root cause: septicemia (systemic bacterial infection in the bloodstream) caused by their own gut bacteria (something easily staved off by a normal immune system).

Time slept and sleep opportunity time should not be confused.

Based on epidemiological data, any adult sleeping on average 6.75 hours a night would be predicted to live only into their early sixties.

Weak immune systems are a known consequence of insufficient sleep.

Concluding that humans need less than 7 hours sleep is a tabloid myth.

Sleeping too much: this data also shows an increased mortality rate once the average sleep amount passes 9 hours. The illusion created is that too much sleep leads to an early death, rather than the more likely conclusion that the illness was just too much despite all the sleep.

There is an adaptive balance to be struck between wakefulness and sleep. In humans that appears 16 hours of wakefulness and 8 hours of sleep, for an average adult.

5 key factors have changed how and how much we sleep: (1) constant electric and LED light, (2) regularized temperature, (3) caffeine, (4) alcohol, and (5) a legacy of punching time cards.

The artificial light that bathes our modern indoors halts the progress of biological time that is normally signaled by the evening surge in melatonin.

Evening electric light winds back our biological clock on average 2 to 3 hours each night.

Light with 8-10 lux intensity has been shown to delay the release of bedtime melatonin in humans. Even the feeblest of lights beats that (bedside lamp: at least 20 lux).

Survey: 90% americans use some form of electronic device 60min or less before bedtime - delaying the biological clock and their ability to produce sleep.

Compared to reading a print book, reading on an iPad suppressed melatonin release by over 50%.

Does reading on iPad change sleep quantity/quality in addition to delaying melatonin release? Yes in 3 ways: individuals (1) lost significant amounts of REM sleeping, (2) felt less rested and sleepier the day after, and (3) suffered a lingering aftereffect of delayed melatonin _for several days after the day when they used the iPad_.

What to do: mood lighting! Remove blue light (red or yellow filters) and maintain complete blackness during the night. (good point for TT2 article)

Alcohol is a sedative: prevents neurons from firing, starting in the prefrontal cortex or frontal lobe region (controlling impulses and behaviour restraints) and then spreading to the rest of the brain. 

Sedation is not sleep: the electrical brainwave state you enter via alcohol is not that of natural sleep - it is more like a light form of anesthesia.

Alcohol fragments sleep, triggering brief awakenings during the night that hinder sleep quality. Most of these awakenings go unnoticed by the sleeper since they don't remember them.

Alcohol is one of the most powerful suppressors of REM sleep we know of. Consuming even moderate amounts of alcohol in mid afternoon or evening means depriving oneself of dream sleep. Extreme state: regular alcohol consumption -> lack of REM sleep -> daytime hallucinations.

One function of REM sleep is to aid in memory integration and association, required for learning. (good point to bring up with older, pre-university students)

The brain has a non-negotiable need for sleep the first night after learning for the purposes of memory processing.

Advice: go to the pub in the morning.

Controlled room temperature: we need core body temperature to decrease by about 1 degree to be able to fall asleep, but in a controlled temperature environment this is harder. If possible, decrease room temperature at night. Other tricks: wash extremities in _warm_ water - the dilated blood vessels help dissipate heat faster.

Enforced awakening: no other species displays the unnatural act of prematurely and artifically terminating sleep.

Individuals waking up to an alarm clock show a spike in blood pressure and shock acceleration in heart rate caused by the response of the fight-or-flight system. It's even worse if you use the snooze button: you get a repeated cardiovascular shock.

Waking up at the same time every day, no matter if it's weekday or weekend, is an important part of a healthy sleep schedule.

Suggested alarm system by Dan Ariely: an alarm clock that is connected by wifi to your bank account, and for every second you remain asleep the alarm will send $10 to an organization... _that you don't like_.

Modern society has taken of of nature's perfect solutions (sleep) and neatly turned it into two problems - a lack thereof at night, and the resulting inability to remain fully awake during the day.

No one would suggest you are awake after taking prescription sleeping pills, but to suggest you are experiencing false sleep would be equally false.

Sleeping pills target the same system in the brain that alcohol does - the receptors that stop your brain cells from firing - and so are part of the same class of drugs, sedatives. Sleeping pills effectively knock out the higher regions of your brain's cortex.

The electrical type of sleep sleeping pills produce is lacking the largest, deepest brainwaves. Plus they ©ome with a bunch of side effects: next-day grogginess, daytime forgetfulness, partial amnesia of nighttime activities and slowed reaction times.

Rebound insomnia: when patients stop taking sleeping pills, they often suffer far worse sleep, sometimes even worse than what led them to taking pills in the first place. This can lead to an addictive cycle.


Sleeping pills are only subjectively effective: patients report sleeping better and falling asleep faster but that's not what sleep recordings show. No statistically significant difference between sleeping pills and placebo in terms of time to fall asleep or soundness of sleep.

In an experiment with sleeping pill-induced sleep (Ambien), drug-sleep failed to solidy memory connections (as sleep in the test group did) and actually caused a 50% weakening of connections formed during the learning period. In other words: drug-induced sleep became a memory eraser rather than engraver.

Kripke, early 2000: from epidemiological data showed a relationship between sleeping pill use and increased mortality risk. Even occasional users (18 pills a year) were 3.6 times more likely to die; heavy users had more than 5x increased risk.

Why the higher death rates? One frequent cause of mortality was higher-than-normal rates of infection (shown before: poor sleep leads to a deficient immune system). Another cause: increased risk for fatal car accidents. Finally: individuals taking sleeping pills were 30 to 40 percent more likely to develop cancer.

No study to date has shown that sleeping pills save lives. 

No medication currently exists with strongly scientifically supported benefits that far outweigh any health risks.

CBT-I (cognitive behavioural therapy for insomnia): new therapy based on basic sleep hygiene principles that is now the first-line treatment for chronic insomnia (not sleeping pills).

Good sleep practices: key one is to go to bed and wake up at the same time every day no matter what.

Sleep and exercise have a clear bidirectional relationship: more physical activity leads to better sleep (although not as tight a relationship as expected), and strong influence of sleep on daytime activity.

Try not to exercise right before bed (at least 2-3 hours before). Reason: harder to drop core body temperature to sleep levels.

Regarding diet:  scientific evidence suggests that you should avoid going to bed too hungry or too full, and avoid diets that are excessively biased towards carbohydrates (over 70% of all energy intake).

It is bizarre that there remains an arrogant culture of uselessness of sleep in many businesses, considering how sensible the professional world is regarding all other areas of employee health, safety and conduct. This mentality has persisted in part because business leaders mistakenly believe that time on task equates with task completion and productivity. This was not true even in the industrial era, is a misguided fallancy and an expensive one, too.

Study in US: lack of sleep costs $2000 per employee per year in lost productivity.

Economic sleep cost hovers between 1-3% of GDP for developed nations (US 2.3%, UK 1.9%, CAN 1.4%, JAP 2.9%, GER 1.6%). Compare that with education spending: (US 5.0%, UK 5.6%, CAN 5.3%, JAP 3.6%, GER 4.9%) (2015 or latest available. Source: [world bank via UNESCO](http://databank.worldbank.org/data/reports.aspx?source=2&series=SE.XPD.TOTL.GD.ZS&country=USA,CAN,JPN,GBR,DEU)). Author claims sleep cost is "almost as much as each country invests in education" - not factually correct (apart from different interpretations of "investment" and "spend") but still, a very significant factor.

Early studies demonstrated that shorter sleep amounts predict lower work rate and slow completion speed of basic tasks.

Suggestion for TT2 academy guide: Webb and Levy, "Effects of spaced and repeated total sleep deprivation", 1984.

A lack of sleep is a causal factor in workplace laziness.

Interesting and dunning-kruger-like twist: participants in these studies do not perceive themselves as applying less effort to the work challenge, or being less effective, when they were sleep deprived - despite both being true.

Individuals like their job less when sleep deprived.

Under-slept employees are less productive, less motivated, less creative, less happy, lazier, and more unethical (measured in propensity for lying or for social loafing).

Of note to those in business: many of these studies report deleterious effects on business outcomes on the basis of only very modest reductions in sleep amount within an invidivual, perhaps twenty to sixty minutes differences between an employee who is honest, creative, innovate, collaborative, and productive, and one who is not.

We often think that a good or bad leader is good or bad day after day - a stable trait. Not true: differences in individual leadership performance fluctuate dramatically from one day to the next, and lack of sleep is one clear contributing factor.

Allowing and encouraging employees, supervisors, and executives to arrive at work well rested turns them from simply looking busy to being productive, honest, useful individuals who inspire, support and help each other. _Ounces of sleep offer pounds of business in return_.

Those who sleep more earn more money on average. US study: 1 hour extra sleep amounts to 4-5% increase in wages. Compare that to a raise which typically yields 2-3%.

The fact of the matter is that most people will trade sleep for a higher salary.

Top companies paying attention: Proctor&Gamble and Goldman Sachs offer free sleep hygiene courses; Nike and Google have relaxed work schedules and set up dedicated nap pods (note: author now works for google [though not at the time of writing]); NASA now has a nap culture based on their observations in astronauts that 20-60 minute naps offered 34% improvement in task performance and >50% increase in overall alertness.

Sleep deprivation used as torture in multiple countries (based on 2007 report). Apart from being inhumane, it is also innefective given how a lack of sleep, even in moderate amounts, degrades every mental faculty necessary to obtain valid information.

Sleep torture: measured on the basis of mortality impact over the long term, it is on par with starvation.

Sleep and the education system: could you really concentrate and learn anything after having forcefully been woken up at 3:15AM, day after day after day? This is the practical effect of early school start times: almost 50% of US highschools start before 7:20AM, meaning pick-up times start 5:45, meaning a teen has to get up at 5:15AM (circadian equivalent to 3:15 in an adult).

The contribution of school start times to this general state of chronic sleep deprivation is especially concerning considering that adolescence is the most susceptible phase of life for developing chronic mental illnesses, such as depression, anxiety, schizophrenia and suicidality.

Admittedly strong but evidence-based statement: unnecessarily bankrupting the sleep of a teenager could make all the difference in the precarious tipping point between psychological wellness and lifelong psychiatric illness.

REM sleep is what stands between rationality and insanity.

Terman book "Genetic Studies of Genius": no matter what the age, the longer a child slept, the more intelectually gifted they were.

Terman believed the swing to an early-morning model of education would deeply damage the intellectual growth of american youth.

Examinations of identical twins show sleep as a powerful factor in altering genetic determinism. Across hundreds of twin pairs, by the age of ten, the twin with the longer sleep pattern was superior in their intellectual and educational abilities.

It is clear that a tired, under-slept brain is little more than a leaky memory sieve, in no state to receive, absorb, or efficiently retain an education. To persist in this way is to handicap our children with partial amnesia.

We (in the american sense, with early-morning schools) are creating a generation of disadvantaged children, hamstrung by a privation of sleep. 

Socio-economic impact: poor children are less likely to have a car, thus rely more on school buses, thus have to get up earlier, putting them at an increased disadvantage.

Research results from schools with later start times: apart from obvious "better, paying more attention", later start times also mean later finish times, protecting many teens from the "danger window" hours which is typically a vulnerable period associated with involvement in crime, alcohol and drug abuse. 

An unexpected result of later start times: the life expectancy of students increased, mostly from sharp decrease in traffic accidents.

Major roadblock towards later start times: culture of "getting kids out the door early in the morning so parents can start work early".

ADHD: symptoms are inability to maintain focus, deficient learning, difficult behaviour, mental health instability. These are nearly identical symptoms as those caused by a lack of sleep. Take an under-slept child to a doctor, describe these symptoms and withhold the lack of sleep, and the doctor will likely diagnose AHDH and prescribe medication for that - amphetamines!, the most powerful drug we know to keep the brain wide awake, and so the last thing the child needs.

Author estimate: more than 50% of children diagnosed with AHDH actually have a sleep disorder.

Well over 70% of parents believe their child gets enough sleep, when in fact only 25% of children (ages 11-18) actually do get the necessary amount.

Sleep deficiency in medicine: residents working 30-hour-straight shift will commit 36% more _serious_ medical errors, compared to those working 16 hours or less.

After a 30 hour shift without sleep, residents make 460% more diagnostic mistakes than when well rested.

One in 20 residents will kill a patient due to lack of sleep.

On surgeries performed by a physician who has had less than 6-hour sleep opportunity the night before, there is 170% increased risk of that surgeon making a serious surgical error.

After 22 hours without sleep, human performance is impaired to the same level as that of someone who is legally drunk. Maybe take that surgery later :)

There is no evidence-based argument for persisting with the current sleep-anemic model of medical training, which cripples the learning, health, and safety of young doctors and patients alike. That it remains this way in the stoic grip of senior medical officials appears to be a clear case of "my mind is made up, don't confuse me with facts".

How to address the lack of sleep problem? Five levels: individual, educational, organizational, public policy and societal. 

Individual level: passive and active solutions. Passive (1) use technology to improve sleep habits. Example: wearable devices that accurately track an individual's sleep and circadian rhythm should be commercially available soon; we can marry these trackers to the revolution of in-home network devices to control sleep-relevant features of the home, like thermostats or lighting, based on sleep patterns. Already existing poor man's versions of this: alarm clock that slowly increases light simulating sunrise, sleep tracking phone apps.

Passive (2) avoid exposure to blue light by installing filters, or by having wavelenght-changing LED bulbs that dynamically change color based on time of day (or sleep patterns froom sleep device), e.g. blue in the morning, warm yellow in the evening. Even possible to use this light-manipulation idea to nudge someone's sleep-wake rhythm, e.g. if you have an early meeting wendesday you can start shifting lights earlier in the week and thus moving you circadian rhythm a little sooner. Same thing for helping with jetlag, commuting environments etc.

Active methods: knowledge is not enough, and one effective way of converting a healthy new habit into a permanent way of life is exposure to your own data. Can be done through these sleep-devices, and more generally through health-tracking devices that include sleep patterns. "Seeing is believing" ensures long-term adherence.

Take a _predictalytics_ view: simulate and show effects of current sleep patterns on patient's body, e.g. testicles shrinking or alzheimer's risk increasing. 

On the educational level: diet, safe sex and physical exercise are all featured in school curricula to some extent. Sleep is not. We could start by developing and implementing a sleep module across schools, with two goals: change the lives of those children and, by having them later communicate those healthy sleep values to their own children, start a positive cycle of sleep habits reinforcement (similar to what happens today with good manners, morality etc).

The cost of delivering such sleep education programs would be a tiny fraction of what we currently pay for our unaddressed global sleep deficit.

Organizational level: Aetna CEO started a program of money incentives (up to $500 a year) for people getting at least 7 hours of sleep a night. Developing a new business culture that takes care of the entire life cycle of an employee is as economicallu prudent as it is compassionate. Seems potentially creepy though.

Author suggestion: instead of modest financial incentives, give time off - both more valued by people and more useful for business since they show up more rested. Suggests a sleep credit system, based not only on sleep amount but also, crucially, on sleep continuity - consistently getting seven to nine hours of sleep opportunity each night, every night.

Flexible work shifts: instead of hard boundaries (9-to-5), set a core window for key interactions (e.g. 12-15) and be flexible on both sides to accomodate all individual chronotypes (which are a continuous spectrum rather than lark/owl binary). Secondary benefits to this approach: rush hour problem is dimished; interactions focused on a core window making it more easy to have "focus time" on the other hours.

The less sleep you have, or the more fragmented, the more sensitive you are to pain of all kinds.

Twelve tips for healthy sleep:
1. Stick to a sleep schedule
2. Exercise - but not too late in the day
3. Avoid caffeine and nicotine
4. Avoid alcohol before bed
5. Avoid large meals and beverages at night
6. Avoid medicines that delay or disrupt sleep
7. Don't nap after 3pm
8. Relax before bed
9. Take a hot bed before bed
10. Dark, cool, gadget-free bedroom
11. Get exposure to sunlight
12. Don't lie in bed awake.







