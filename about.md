---
layout: page
title: About
permalink: /about/
---

I'm João and this is my personal page. Its purpose is to be a space for me to share things with myself, although I hope some of those will be useful to other people as well. I keep it public because it keeps me accountable, and I strive to make it simple, clear and honest.

Currently I'm a data scientist working in digital marketing. In the past I've been a researcher, a basketball coach and a physics student. I'm interested in using data to understand and contribute to solving meaningful problems.

Originally from Lisbon, Portugal.
